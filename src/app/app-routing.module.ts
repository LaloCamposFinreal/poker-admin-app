import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainLayoutComponent } from './layout/main-layout/main-layout.component';


const routes: Routes = [
  //  {
  //   path: 'login',
  //     loadChildren: () =>
  //     import('./login/login.module').then(
  //       m => m.LoginModule
  //     )
  // },
  // {
  //   path: '',
  //   redirectTo: '/login',
  //   pathMatch: 'full'
  // },
  // {
  //   path: '**',
  //   redirectTo: '/login'
  // },
  {
    path: 'home',
    component: MainLayoutComponent,    
    loadChildren: () =>
      import('./home/home.module').then(
        m => m.HomeModule
      )
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, {
      // useHash: true,
      // onSameUrlNavigation: 'reload',
      scrollPositionRestoration: 'top'
    })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
